import './feature.scss';
import {useState} from "react";
import FFeaturePictogram from "../pictograms/features/feature-pictogram-factory";

export default function Feature(props) {
    const [content] = useState(props.content);

    return (
        <div className="feature-item-container">
            {FFeaturePictogram(content.logo, "feature-item-logo")}
            <p className="feature-item-title">{content.title}</p>
            <p className="feature-item-description">{content.description}</p>
        </div>
    );
}

// TODO: implement proptypes