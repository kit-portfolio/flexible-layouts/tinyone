import React from "react";
import {ReactComponent as BookmarkIcon} from "./bookmark.svg";
import {ReactComponent as CodingIcon} from "./coding.svg";
import {ReactComponent as EmailIcon} from "./email.svg";
import {ReactComponent as FolderIcon} from "./folder.svg";
import {ReactComponent as PhoneIcon} from "./phone.svg";
import {ReactComponent as PsdIcon} from "./psd.svg";

export default function FFeaturePictogram(feature, className) {
    switch (feature) {
        case "bookmark" : return <BookmarkIcon className={className} />;
        case "coding" : return <CodingIcon className={className} />;
        case "email" : return <EmailIcon className={className} />;
        case "folder" : return <FolderIcon className={className} />;
        case "phone" : return <PhoneIcon className={className} />;
        case "psd" : return <PsdIcon className={className} />;
        default: return null;
    }
}