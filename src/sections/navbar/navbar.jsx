import React from "react";
import {Link} from "react-router-dom";
import './navbar.scss';
import {ReactComponent as Logo} from "../../components/pictograms/logo.svg";

export default function Navbar() {
    return (
        <section className="navbar-section">
            <navbar className="navbar-container">
                    <Logo className="navbar-logo"/>
                    <h2 className="navbar-title">tinyone</h2>
                <div className="navbar-menu-box">
                    <Link to="/features" className="navbar-menu-item">Features</Link>
                    <Link to="/support" className="navbar-menu-item">Support</Link>
                    <Link to="/blog" className="navbar-menu-item">Blog</Link>
                </div>
            </navbar>
        </section>
    );
}