import React, {useState, useEffect} from "react";
import './features.scss';
import Feature from "../../components/feature/feature";

export default function Features() {
    const [features, setFeatures] = useState([]);

    useEffect(() => {
        fetch('mocks/feature-list.json')
            .then(res => res.json())
            .then(data => setFeatures(data.data))
    }, [])

    return (
        <section className="features-section">
            <h2 className="features-header">Tinyone features</h2>
            <p className="features-subheader">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae
                eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.</p>
            <div className="features-container">
                {features.map((item, index) => <Feature
                    content={item}
                    key={index}
                />)}
            </div>
        </section>
    );
}
