import React from "react";
import './cover.scss';
import {ReactComponent as AndroidIcon} from "../../components/pictograms/os/android.svg";
import {ReactComponent as AppleIcon} from "../../components/pictograms/os/apple.svg";
import {ReactComponent as WindowsIcon} from "../../components/pictograms/os/windows.svg";
import {ReactComponent as DownloadIcon} from "../../components/pictograms/download.svg";
import {ReactComponent as Logo} from "../../components/pictograms/logo.svg";

export default function Cover() {

    function handleDownloadButtonClick() {
        alert('Stub');
    }

    return (
        <section className="cover-section">
            <div className="cover-info-container">
                <h2 className="cover-title">Inspire your inspiration</h2>
                <h3 className="cover-subheader">Simple to use for your app, products showcase and your inspiration</h3>
                <p className="cover-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros
                    eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis. Proin viverra risus a eros
                    volutpat tempor. In quis arcu et eros porta lobortis sit </p>
                <div className="cover-download-container">
                    <AppleIcon className="cover-os-icon"/>
                    <AndroidIcon className="cover-os-icon"/>
                    <WindowsIcon className="cover-os-icon"/>
                    <div className="cover-download-button" onClick={() => handleDownloadButtonClick()}>
                        download
                        <DownloadIcon className="cover-download-icon"/>
                    </div>
                </div>
            </div>
            <div className="cover-media-container">
                <div className="cover-mobile-phone">
                    <div className="cover-phone-speaker" />
                    <div className="cover-mobile-phone-screen">
                        <Logo className="cover-phone-logo" />
                        <h2 className="cover-phone-title">tinyone</h2>
                    </div>
                    <div className="cover-phone-button" />
                </div>
            </div>
        </section>
    );
}

// TODO: implement slider?