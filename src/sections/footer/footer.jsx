import React from "react";
import {Link} from "react-router-dom";
import {useFormik} from 'formik';
import './footer.scss';
import {ReactComponent as FacebookIcon} from "../../components/pictograms/social/facebook.svg";
import {ReactComponent as TwitterIcon} from "../../components/pictograms/social/twitter.svg";
import {ReactComponent as GoogleIcon} from "../../components/pictograms/social/google-plus.svg";
import {ReactComponent as VimeoIcon} from "../../components/pictograms/social/vimeo.svg";

export default function Footer() {
    const formik = useFormik({
        initialValues: {
            email: '',
        },
        onSubmit: (values, {resetForm}) => {
            alert(`Subscription stub (${values.email})`);
            resetForm({})
        },
    });

    return (
        <section className="footer-section">
            <h2 className="footer-title">Keep in touch with us</h2>
            <p className="footer-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget
                tellus tristique bibendum. Donec rutrum sed sem quis venenatis.</p>
            <form onSubmit={formik.handleSubmit}
                  className="footer-email-form">
                <input
                    id="email"
                    name="email"
                    type="email"
                    className="footer-email-input"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                    placeholder="Enter your email"
                />
                <input
                    type="submit"
                    className="footer-email-submit"
                    value="submit"
                />
            </form>
            <div className="footer-social-buttons-container">
                <Link to="#">
                    <FacebookIcon className="footer-social-button"/>
                </Link>
                <Link to="#">
                    <TwitterIcon className="footer-social-button"/>
                </Link>
                <Link to="#">
                    <GoogleIcon className="footer-social-button"/>
                </Link>
                <Link to="#">
                    <VimeoIcon className="footer-social-button"/>
                </Link>
            </div>
        </section>
    );
}

// TODO: Implement Formik, add formik to stack