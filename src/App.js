import React from "react";
import "./theme/common.scss";
import Cover from "./sections/cover/cover";
import Features from "./sections/features/features";
import Footer from "./sections/footer/footer";
import Navbar from "./sections/navbar/navbar";

export default function App() {
    return (
        <div className="App">
            <Navbar/>
            <Cover/>
            <Features/>
            <Footer/>
        </div>
    );
}
