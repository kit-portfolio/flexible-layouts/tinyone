# Tinyone

### Tech stack
* Responsive.
  * S: 320px - 480px
  * M: 480px - 995px
  * L: over 995px 
* React
* React Router
* Formik
* Mobile-first
* SaSS

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/flexible-layouts/tinyone/).

### Launch
To run the project follow the next steps:
* Install node modules
* Run `yarn start` or `npm run start`
* Enjoy

# Preview
![Project preview](preview.png#center)